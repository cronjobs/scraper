from urllib.parse import urlencode, urlsplit

import scrapy


class CountryfoodSpider(scrapy.Spider):
    name = 'countryfood'
    allowed_domains = ['localhost']
    start_urls = ['http://localhost:5000/']

    def parse(self, response):
        # Check if we need to log in.
        # After logging in, the server redirects to index.
        if urlsplit(response.request.url).path == '/login':
            yield response.follow(
                response.css("form::attr(action)").get(),
                method='POST', body=urlencode({
                    "username": "admin",
                    "password": "admin",
                }), callback=self.parse_index, headers={
                    "Content-Type": "application/x-www-form-urlencoded",
                })
        # If we don't have to log in, then we are already in the index page.
        else:
            yield from self.parse_index(response)

    def parse_index(self, response):
        yield response.follow(
            response.css("p > a::attr(href)").get(),
            callback=self.parse_data,
        )

    def parse_data(self, response):
        for tr in response.css("table.food-table tbody tr"):
            tds = tr.css("td")
            yield {
                "country": tds[0].css('::text').get(),
                "income": tds[1].css('::text').get(),
                "happiness": tds[2].css('::text').get(),
                "food": [i.strip() for i in tds[3].css('::text').extract() if i.strip()],
            }
