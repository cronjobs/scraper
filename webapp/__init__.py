from datetime import datetime
from functools import wraps

from flask import Flask, request, redirect, session, url_for, render_template

app = Flask(__name__, template_folder="templates")
app.config["SECRET_KEY"] = "secret secret key"

dummy_data = [
    {
        "country": "Canada",
        "gdp": "High",
        "happiness": "High",
        "food": [
            "Elk",
            "Mushrooms",
            "Peanut butter",
            "Ham",
            "Crossaints",
        ]
    },
    {
        "country": "America",
        "gdp": "High",
        "happiness": "Medium",
        "food": [
            "Beef",
            "Beef",
            "Beef",
            "Ham",
            "Sugar",
            "Sugar"
        ]
    },
    {
        "country": "Uganda",
        "gdp": "Low",
        "happiness": "High",
        "food": [
            "Rice",
            "Beef",
            "Bananas",
            "Lion meet"
        ]
    },
    {
        "country": "India",
        "gdp": "Medium",
        "happiness": "Medium",
        "food": [
            "Rice",
            "Daal",
            "Potatoes",
            "Chicken",
            "Beef",
            "Spinach",
            "Fish",
            "Fish",
            "Milk",
            "Milk",
            "Milk"
            "Spices"
        ]
    },
    {
        "country": "Russia",
        "gdp": "High",
        "happiness": "High",
        "food": [
            "Ham",
            "Mayonnaise",
            "Fish",
            "Ice",
            "Bread",
            "Vodka",
            "Vodka"
        ]
    }
]


def login_required(viewfunc):
    @wraps(viewfunc)
    def decorate(*args, **kwargs):
        if "logged_in" not in session:
            return redirect(url_for("login"))
        return viewfunc(*args, **kwargs)
    return decorate


@app.route("/")
@app.route("/index")
@login_required
def index():
    return render_template('index.html')


@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    if request.form.get("username") == "admin" and request.form.get("password") == "admin":
        session["logged_in"] = str(datetime.today)
        return redirect(url_for("index"))
    return """
    Login failed. Go <a href="{}">back</a>?
    """.format(url_for("login"))


@app.route("/food_by_country")
@login_required
def food_by_country():
    return render_template("food_by_country.html", data=dummy_data)


if __name__ == '__main__':
    app.run(debug=True)